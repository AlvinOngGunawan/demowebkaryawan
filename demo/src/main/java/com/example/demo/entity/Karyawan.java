package com.example.demo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Data
@NoArgsConstructor
@Entity
@Table(name = "karyawan_master")
public class Karyawan {
    @Id
    private int nik;
    private String nama;
    private String ttl;
    private String jenis_kelamin;
    private String kota;
    private String rtrw;
    private String kelurahan;
    private String kecamatan;
    private String agama;
    private String status;
    private String pekerjaan;
    private String kewarganegaraan;

    public int getNik() {
        return nik;
    }

    public String getNama() {
        return nama;
    }

    public String getTtl() {
        return ttl;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public String getKota() {
        return kota;
    }

    public String getRtrw() {
        return rtrw;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public String getAgama() {
        return agama;
    }

    public String getStatus() {
        return status;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public String getKewarganegaraan() {
        return kewarganegaraan;
    }


    public void setNik(int nik) {
        this.nik = nik;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setRtrw(String rtrw) {
        this.rtrw = rtrw;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public void setKewarganegaraan(String kewarganegaraan) {
        this.kewarganegaraan = kewarganegaraan;
    }
}
