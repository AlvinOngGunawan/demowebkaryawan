package com.example.demo.repository;

import com.example.demo.entity.Karyawan;
import org.springframework.data.repository.CrudRepository;

public interface KaryawanRepository extends CrudRepository<Karyawan, Integer> {
    Karyawan findByNama(String nama);
}
