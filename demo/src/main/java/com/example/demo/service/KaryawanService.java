package com.example.demo.service;

import com.example.demo.entity.Karyawan;
import com.example.demo.repository.KaryawanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class KaryawanService {
    @Autowired
    private KaryawanRepository repository;

    public Karyawan saveKaryawan(Karyawan karyawan) {
        return repository.save(karyawan);
    }

    public List<Karyawan> saveKaryawans(List<Karyawan> karyawan) {
        return (List<Karyawan>) repository.saveAll(karyawan);
    }

    public List<Karyawan> getKaryawans() {
        return (List<Karyawan>) repository.findAll();
    }

    public Karyawan getKaryawanById(int id) {
        return repository.findById(id).orElse(null);
    }

    public Karyawan getKaryawanByNama(String nama) {
        return repository.findByNama(nama);
    }

    public String deleteKaryawan(int nik) {
        repository.deleteById(nik);
        return "Karyawan removed !!";
    }


//    public Product updateProduct(Product product) {
//        Product existingProduct = repository.findById(product.getID()).orElse(null);
//        existingProduct.setName(product.getName());
//        existingProduct.setQty(product.getQty());
//        existingProduct.setPrice(product.getPrice());
//        return repository.save(existingProduct);

    //    }
   public Karyawan
    updateKaryawan(Karyawan karyawan) {
        Karyawan existingKaryawan = repository.findById(karyawan.getNik()).orElse(null);
        existingKaryawan.setNama(karyawan.getNama());
        existingKaryawan.setTtl(karyawan.getTtl());
        existingKaryawan.setJenis_kelamin(karyawan.getJenis_kelamin());
        existingKaryawan.setKota(karyawan.getKota());
        existingKaryawan.setRtrw(karyawan.getRtrw());
        existingKaryawan.setKelurahan(karyawan.getKelurahan());
        existingKaryawan.setKecamatan(karyawan.getKecamatan());
        existingKaryawan.setAgama(karyawan.getAgama());
        existingKaryawan.setStatus(karyawan.getStatus());
        existingKaryawan.setPekerjaan(karyawan.getPekerjaan());
        existingKaryawan.setKewarganegaraan(karyawan.getKewarganegaraan());
        return repository.save(existingKaryawan);
    }
}

