function addData() {
    let nik = document.getElementById("nik").value;
    let nama = document.getElementById("nama").value;
    let ttl = document.getElementById("tempat_lahir").value;
    let jenis_kelamin = document.getElementById("jenis_kelamin").value;
    let agama = document.getElementById("agama").value;
    let rtrw = document.getElementById("rtrw").value;
    let kelurahan = document.getElementById("kelurahan").value;
    let kota = document.getElementById("kota").value;
    let kecamatan = document.getElementById("kecamatan").value;
    let golongan_darah = document.getElementById("golongan_darah").value;
    let status = document.getElementById("status_perkawinan").value;
    let pekerjaan = document.getElementById("pekerjaan").value;
    let kewarganegaraan = document.getElementById("kewarganegaraan").value;


    let jsonStr = JSON.stringify({
        nik: parseInt(nik),
        nama: nama,
        ttl: ttl,
        jenis_kelamin: jenis_kelamin,
        golongan_darah: golongan_darah,
        kota: kota,
        rtrw: rtrw,
        kelurahan: kelurahan,
        kecamatan: kecamatan,
        pekerjaan: pekerjaan,
        agama: agama,
        status: status,
        kewarganegaraan: kewarganegaraan
    });

    fetch(`http://localhost:8080/addKaryawan`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonStr,
    }).then(response => { return response.json() })
        .then(data => {
            console.log(data)
            console.log('Success:', data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });

};
function updateData(nik) {
    fetch(`http:localhost:8080/${nik}`), {
        method: `PUT`,
    }
    
}
function getByIdUpdate(nik) {
    fetch(`http://localhost:8080/Karyawan/${nik}`)
      .then(response => response.json())
      .then(data => updateData(data))
      .catch(console.error); 
  }
  
  function updateData(res) {
     const fieldUpdate = document.getElementById("selectUpdate").value;
     const inputUpdate = document.getElementById("inputUpdate").value;
     
    res[fieldUpdate] = inputUpdate;
    
    if (fieldUpdate != "") {
      fetch(`http://localhost:8989/update`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(res)
      })
      .then(res => location.reload())
      .catch(console.error);
    }
   }
function deleteData(nik) {
    fetch(`http://localhost:8080/delete/${nik}`, {
        method: `DELETE`,
    }).then(window.location.reload()
        .catch(console.error)
    )
}

function getData(url) {
    let http = new XMLHttpRequest();
    http.open("GET", url, true)
    http.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let res = JSON.parse(this.responseText)
            console.log(res.length)

            let msgReceipt = ""
            let no = 1;
            for (let i = 0; i < res.length; i++) {
                msgReceipt += `
<tr>
                <td>${no}</td>
                <td>${response[i].nik}</td>
                <td>${response[i].nama}</td>
                <td>${response[i].jenis_kelamin}</td>
                <td>${response[i].pekerjaan}</td>
                <td>${response[i].kewarganegaraan}</td>
                <td>
                    
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#${res[i].name}Detail" >
                    DETAIL
                </button>

                <div class="modal fade" id="${res[i].name}Detail" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">DETAIL DATA EMPLOYEE</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="nik">NIK</label>
                                        <br>
                                        ${res[i].nik}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="name">NAMA</label>
                                        <br>
                                        ${res[i].name}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="tempat_lahir">TEMPAT LAHIR</label>
                                        <br>
                                        ${res[i].tempat_lahir}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="tanggal_lahir">TANGGAL LAHIR</label>
                                        <br>
                                        ${res[i].tanggal_lahir}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="jenis_kelamin">JENIS KELAMIN</label>
                                        <br>
                                       ${res[i].jenis_kelamin}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="golongan_darah">GOLONGAN DARAH</label>
                                        <br>
                                        ${res[i].golongan_darah}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="alamat">ALAMAT</label>
                                        <br>
                                        ${res[i].alamat}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="rt">RT</label>
                                        <br>
                                        ${res[i].rt}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="rw">RW</label>
                                        <br>
                                        ${res[i].rw}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kelurahan">KELURAHAN</label>
                                        <br>
                                        ${res[i].kelurahan}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kecamatan">KECAMATAN</label>
                                        <br>
                                        ${res[i].kecamatan}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="agama">AGAMA</label>
                                        <br>
                                        ${res[i].agama}
                                    </div>
                                    <label style="font-weight: bold" for="status_perkawinan">STATUS PERKAWINAN</label>
                                    <br>
                                    <div class="form-group">
                                        ${res[i].status_perkawinan}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="pekerjaan">PEKERJAAN</label>
                                        <br>
                                        ${res[i].pekerjaan}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kewarganegaraan">KEWARGANEGARAAN</label>
                                        <br>
                                        ${res[i].kewarganegaraan}
                                    </div>

                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>



                     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#updateData" >
                    UPDATE
                </button>

                <div class="modal fade" id="updateData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">UPDATE DATA EMPLOYEE</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" value="${res[i].id}" id="id">
                                <form style="font-weight: bold">
                                    <div class="form-group">
                                        <label for="edit-nik">NIK</label>
                                        <input type="text" class="form-control" id="edit-nik" autofocus value="${res[i].nik}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-name">NAMA</label>
                                        <input type="text" class="form-control" id="edit-name" value="${res[i].name}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-tempat_lahir">TEMPAT LAHIR</label>
                                        <input type="text" class="form-control" id="edit-tempat_lahir" value="${res[i].tempat_lahir}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-tanggal_lahir">TANGGAL LAHIR</label>
                                        <input type="date" class="form-control" id="edit-tanggal_lahir" value="${res[i].tanggal_lahir}" />
                                    </div>

                                    <div class="form-group">
                                        <label for="edit-jenis_kelamin">JENIS KELAMIN</label>
                                        <select class="custom-select" id="edit-jenis_kelamin">
                                            <option value="${res[i].jenis_kelamin}">${res[i].jenis_kelamin}</option>
                                            <option value="LAKI-LAKI">Laki-Laki</option>
                                            <option value="PEREMPUAN">Perempuan</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="edit-golongan_darah">GOLONGAN DARAH</label>
                                        <select class="custom-select" id="edit-golongan_darah">
                                            <option value="${res[i].golongan_darah}">${res[i].golongan_darah}</option>
                                            <option value="A"">A</option>
                                            <option value="B"">B</option>
                                            <option value="AB"">AB</option>
                                            <option value="I"">O</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-alamat">ALAMAT</label>
                                        <input type="text" class="form-control" id="edit-alamat" value="${res[i].alamat}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-rt">RT</label>
                                        <input type="text" class="form-control" id="edit-rt" value="${res[i].rt}" />
                                    </div>
                                     <div class="form-group">
                                        <label for="edit-rw">RW</label>
                                        <input type="text" class="form-control" id="edit-rw" value="${res[i].rw}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-kelurahan">KELURAHAN</label>
                                        <input type="text" class="form-control" id="edit-kelurahan" value="${res[i].kelurahan}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-kecamatan">KECAMATAN</label>
                                        <input type="text" class="form-control" id="edit-kecamatan" value="${res[i].kecamatan}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-agama">AGAMA</label>
                                        <select class="custom-select" id="edit-agama">
                                            <option value="${res[i].agama}">${res[i].agama}</option>
                                            <option value="ISLAM">ISLAM</option>
                                            <option value="PROTESTAN">PROTESTAN</option>
                                            <option value="KATOLIK">KATOLIK</option>
                                            <option value="HINDU">HINDU</option>
                                            <option value="BUDDHA">BUDDHA</option>
                                            <option value="KHONGHUCU">KHONGHUCU</option>
                                        </select>
                                    </div>
                                    <label for="edit-status_perkawinan">STATUS PERKAWINAN</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="edit-status_perkawinan" >
                                            <option value="${res[i].status_perkawinan}">${res[i].status_perkawinan}</option>
                                            <option value="BELUM MENIKAH">BELUM MENIKAH</option>
                                            <option value="SUDAH MENIKAH">SUDAH MENIKAH</option>
                                            <option value="SUDAH PERNAH MENIKAH">SUDAH PERNAH MENIKAH
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="edit-pekerjaan">PEKERJAAN</label>
                                        <input type="text" class="form-control" id="edit-pekerjaan" value="${res[i].pekerjaan}" />
                                    </div>

                                    <div class="form-group">
                                        <label for="edit-kewarganegaraan">KEWARGANEGARAAN</label>
                                        <select class="custom-select" id="edit-kewarganegaraan">
                                            <option value="${res[i].kewarganegaraan}">${res[i].kewarganegaraan}</option>
                                            <option value="WNI">WNI</option>
                                            <option value="WNA">WNA</option>
                                        </select>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                            
                                        <button type="submit" class="btn btn-primary" onclick="updateData()">Update Data</button>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>


                    <button type="button" class="btn btn-danger" onclick="deleteData(${res[i].id})">DELETE</button>
</td>
</tr>
                `
                no++;
            }
            document.getElementById("tableData").innerHTML = msgReceipt
        }
    }
    http.send()
}
// function getData(url) {
//     let http = new XMLHttpRequest();
//     http.open("GET", url, true)
//     http.onreadystatechange = function () {
//         if (this.readyState === 4 && this.status === 200) {
//             let response = JSON.parse(this.responseText)

//             let msgReceipt = ""
//             let no = 1;
//             for (let i = 0; i < response.length; i++) {
//                 console.log(response[i]);
//                 msgReceipt += `
//                 <tr>
//                 <td>${no}</td>
//                 <td>${response[i].nik}</td>
//                 <td>${response[i].nama}</td>
//                 <td>${response[i].jenis_kelamin}</td>
//                 <td>${response[i].pekerjaan}</td>
//                 <td>${response[i].kewarganegaraan}</td>
//                 <td>
                        

//                 <div class="modal fade" id="${response[i].nama}" tabindex="-1" role="dialog"
//                      aria-labelledby="exampleModalLabel" aria-hidden="true">
//                     <div class="modal-dialog">
//                         <div class="modal-content">
//                             <div class="modal-header">
//                                 <h5 class="modal-title" id="exampleModalLabel">DETAIL EMPLOYEE</h5>
//                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
//                                     <span aria-hidden="true">&times;</span>
//                                 </button>
//                             </div>
//                             <div class="modal-body">

//                                 <form>
//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="nik">NIK</label>
//                                         <br>
//                                         ${response[i].nik}
//                                     </div>
//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="nama">NAMA</label>
//                                         <br>
//                                         ${response[i].nama}
//                                     </div>
//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="tanggal_lahir">TANGGAL LAHIR</label>
//                                         <br>
//                                         ${response[i].ttl}
//                                     </div>

//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="jenis_kelamin">JENIS KELAMIN</label>
//                                         <br>
//                                        ${response[i].jenis_kelamin}
//                                     </div>

//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="golongan_darah">GOLONGAN DARAH</label>
//                                         <br>
//                                         ${response[i].golongan_darah}
//                                     </div>
//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="alamat">ALAMAT</label>
//                                         <br>
//                                         ${response[i].kota}
//                                     </div>
//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="rtrw">RT/RW</label>
//                                         <br>
//                                         ${response[i].rtrw}
//                                     </div>
//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="kelurahan">KELURAHAN</label>
//                                         <br>
//                                         ${response[i].kelurahan}
//                                     </div>
//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="kecamatan">KECAMATAN</label>
//                                         <br>
//                                         ${response[i].kecamatan}
//                                     </div>
//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="agama">AGAMA</label>
//                                         <br>
//                                         ${response[i].agama}
//                                     </div>
//                                     <label style="font-weight: bold" for="status_perkawinan">STATUS PERKAWINAN</label>
//                                     <br>
//                                     <div class="form-group">
//                                         ${response[i].status}
//                                     </div>

//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="pekerjaan">PEKERJAAN</label>
//                                         <br>
//                                         ${response[i].pekerjaan}
//                                     </div>

//                                     <div class="form-group">
//                                         <label style="font-weight: bold" for="kewarganegaraan">KEWARGANEGARAAN</label>
//                                         <br>
//                                         ${response[i].kewarganegaraan}
//                                     </div>

//                                     <div class="modal-footer">
//                                         <button type="reset" class="btn btn-secondary" data-dismiss="modal">
//                                             Close
//                                         </button>
//                                     </div>
//                                 </form>
                            
//                             </div>
//                         </div>
//                     </div>
//                 </div>

//             </form>
//                 <td><div class="modal-footer">
//                 <button type="button" class="btn btn-primary " onclick='editData(${response[i].nik})'>Update</button>
//                 <button type="button" class="btn btn-secondary " data-toggle="modal" data-target="#${response[i].nama}">
//                                         DETAIL
//                                     </button>
//                 <button type='button' class='btn btn-danger' onclick='deleteData(${response[i].nik})'>Delete</button>
//               </div></td>
//                 </tr>
//                 `
//                 no++;
//             }
//             document.getElementById("tableData").innerHTML = msgReceipt
//         }
//     }
//     http.send()
// }

// getData("http://localhost:8080/Karyawans")
// function editData(url) {
//     var updateModal = document.getElementById("updateData")
//     updateModal.innerHTML = `<div class="modal-dialog">
//                                 <div class="modal-content">
//                                     <div class="modal-header">
//                                         <h5 class="modal-title">Edit Data Karyawan</h5>
//                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
//                                             <span aria-hidden="true">&times;</span>
//                                         </button>
//                                     </div>
//                                     <div class="modal-body">
//                                         <input type="hidden" nik="nik" value="${response[i].nik}">
//                                         <div class="form-group">
//                                             <label for="edit-nama">NAMA</label>
//                                             <input type="number" class="form-control" id="edit-nama" value="${response[i].nama}" readonly>
//                                         </div>
//                                         <div class="form-group">
//                                             <label for="edit-ttl">TTL</label>
//                                             <input type="text" class="form-control" id="edit-ttl" value="${response[i].ttl}">
//                                         </div>
//                                         <div class="form-group">
//                                             <div class="row">
//                                                 <div class="col">
//                                                     <label for="edit-kota">Kota</label>
//                                                     <input type="text" class="form-control" id="edit-kota" value="${response[i].kota}">
//                                                 </div>
//                                                 <div class="col">
//                                                     <label for="edit-rtrw">RT/RW</label>
//                                                     <input type="text" class="form-control" id="edit-rtrw" value="${response[i].rtrw}">
//                                                 </div>
//                                             </div>
//                                         </div>
//                                         <div class="form-group">
//                                             <label for="edit-jenis-kelamin">JENIS KELAMIN</label>
//                                             <select class="form-control" id="edit-jenis-kelamin">
//                                                 <option value="${response[i].jenis_kelamin}">${response[i].jenis_kelamin}</option>
//                                                 <option value="Pria">Pria</option>
//                                                 <option value="Wanita">Wanita</option>
//                                             </select>
//                                         </div>
//                                         <div class="form-group">
//                                             <label for="edit-kelurahan">KELURAHAN</label>
//                                             <input type="text" class="form-control" id="edit-kelurahan" value="${response[i].kelurahan}">
//                                         </div>
//                                         <div class="form-group">
//                                             <div class="row">
//                                                 <div class="col">
//                                                     <label for="edit-kecamatan">KECAMATAN</label>
//                                                     <input type="text" class="form-control" id="edit-kecamatan" value="${response[i].kecamatan}">
//                                                 </div>
//                                                 <div class="col">
//                                                     <label for="edit-kewarganegaraan">KEWARGANEGARAAN</label>
//                                                     <input type="text" class="form-control" id="edit-kewarganegaraan" value="${response[i].kewarganegaraan}">
//                                                 </div>
//                                             </div>
//                                         </div>
//                                         <div class="form-group">
//                                             <div class="row">
//                                                 <div class="col">
//                                                     <label for="edit-status">STATUS</label>
//                                                     <input type="text" class="form-control" id="edit-status" value="${response[i].status}">
//                                                 </div>
//                                                 <div class="col">
//                                                     <label for="edit-pekerjaan">PEKERJAAN</label>
//                                                     <input type="text" class="form-control" id="edit-pekerjaan" value="${response[i].pekerjaan}">
//                                                 </div>
//                                             </div>
//                                         </div>
//                                         <div class="form-group">
//                                             <label for="edit-agama">AGAMA</label>
//                                             <select class="form-control" id="edit-agama">
//                                                 <option value="${response[i].agama}">${response[i].agama}</option>
//                                                 <option value="Islam">Islam</option>
//                                                 <option value="Protestan">Protestan</option>
//                                                 <option value="Katolik">Katolik</option>
//                                                 <option value="Hindu">Hindu</option>
//                                                 <option value="Buddha">Buddha</option>
//                                                 <option value="Khonghucu">Khonghucu</option>
//                                             </select>
//                                         </div>
//                                         <br>
//                                         </div>
//                                         <div class="modal-footer">
//                                             <button type="submit" class="btn btn-primary" id="submit" onclick="updateData()">Submit</button>
//                                         </div>
//                                     </div>
//                                 </div>`
// }

function updateData() {
    var nik = document.getElementById("edit-nik").value
    var nama = document.getElementById("edit-nama").value
    var ttl = document.getElementById("edit-ttl").value
    var jenis_kelamin = document.getElementById("edit-jenis-kelamin").value
    var kota = document.getElementById("edit-kota").value
    var rtrw = document.getElementById("edit-rtrw").value
    var kelurahan = document.getElementById("edit-kelurahan").value
    var kecamatan = document.getElementById("edit-kecamatan").value
    var agama = document.getElementById("edit-agama").value
    var pekerjaan = document.getElementById("edit-pekerjaan").value
    var kewarganegaraan = document.getElementById("edit-kewarganegaraan").value

    if (nama == "" || ttl == "" || jenis_kelamin == "" || kota == "" || rtrw == "" || kelurahan == "" || kecamatan == "" || agama == "" || pekerjaan == "" || kewarganegaraan == "") {
        alert("data tidak boleh kosong")
    }else{
        var str_json = JSON.stringify({
            id: id,
            nik: nik,
            nama: nama,
            ttl: ttl,
            jenis_kelamin: jenis_kelamin,
            kota: kota,
            rtrw: rtrw,
            kelurahan: kelurahan,
            kecamatan: kecamatan,
            agama: agama,
            pekerjaan: pekerjaan,
            kewarganegaraan: kewarganegaraan
        })
    }
}