function addData() {
    let nik = document.getElementById("nik").value;
    let nama = document.getElementById("nama").value;
    let ttl = document.getElementById("ttl").value;
    let jenis_kelamin = document.getElementById("jenis_kelamin").value;
    let agama = document.getElementById("agama").value;
    let rtrw = document.getElementById("rtrw").value;
    let kelurahan = document.getElementById("kelurahan").value;
    let kota = document.getElementById("kota").value;
    let kecamatan = document.getElementById("kecamatan").value;
    let status = document.getElementById("status").value;
    let pekerjaan = document.getElementById("pekerjaan").value;
    let kewarganegaraan = document.getElementById("kewarganegaraan").value;


    let jsonStr = JSON.stringify({
        nik: parseInt(nik),
        nama: nama,
        ttl: ttl,
        jenis_kelamin: jenis_kelamin,
        kota: kota,
        rtrw: rtrw,
        kelurahan: kelurahan,
        kecamatan: kecamatan,
        pekerjaan: pekerjaan,
        agama: agama,
        status: status,
        kewarganegaraan: kewarganegaraan
    });

    fetch('http://localhost:8080/addKaryawan', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonStr,
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

function deleteData(nik) {
    let del = confirm("yakin ingin menghapus data?")
    if (del) {
        fetch(`http://localhost:8080/delete/${nik}`, {
            method: 'DELETE',
        })
            .then(() => location.reload())
            .catch(console.error)
    }
}

function getData(url) {
    let http = new XMLHttpRequest();
    http.open("GET", url, true)
    http.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let res = JSON.parse(this.responseText)
            console.log(res.length)

            let msgReceipt = ""
            let no = 1;
            for (let i = 0; i < res.length; i++) {
                msgReceipt += `
<tr>
                    <td>${no}</td>
                    <td>${res[i].nik}</td>
                    <td>${res[i].nama}</td>
                    <td>${res[i].jenis_kelamin}</td>
                    <td>${res[i].pekerjaan}</td>
                    <td>${res[i].kewarganegaraan}</td>
                    <td>
                    
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#${res[i].nama}Detail" >
                    DETAIL
                </button>

                <div class="modal fade" id="${res[i].nama}Detail" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">DETAIL DATA EMPLOYEE</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="nik">NIK</label>
                                        <br>
                                        ${res[i].nik}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="name">NAMA</label>
                                        <br>
                                        ${res[i].nama}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="tempat_lahir">TEMPAT LAHIR</label>
                                        <br>
                                        ${res[i].ttl}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="jenis_kelamin">JENIS KELAMIN</label>
                                        <br>
                                       ${res[i].jenis_kelamin}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kota">KOTA</label>
                                        <br>
                                        ${res[i].kota}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="rtrw">RT/RW</label>
                                        <br>
                                        ${res[i].rtrw}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kelurahan">KELURAHAN</label>
                                        <br>
                                        ${res[i].kelurahan}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kecamatan">KECAMATAN</label>
                                        <br>
                                        ${res[i].kecamatan}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="agama">AGAMA</label>
                                        <br>
                                        ${res[i].agama}
                                    </div>
                                    <label style="font-weight: bold" for="status">STATUS PERKAWINAN</label>
                                    <br>
                                    <div class="form-group">
                                        ${res[i].status}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="pekerjaan">PEKERJAAN</label>
                                        <br>
                                        ${res[i].pekerjaan}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="kewarganegaraan">KEWARGANEGARAAN</label>
                                        <br>
                                        ${res[i].kewarganegaraan}
                                    </div>

                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>



                     <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#${res[i].nama}" >
                    UPDATE
                </button>

                <div class="modal fade" id="${res[i].nama}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">UPDATE DATA EMPLOYEE</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" value="${res[i].id}" id="id">
                                <form style="font-weight: bold">
                                    <div class="form-group">
                                        <label for="edit-nik">NIK</label>
                                        <input type="text" class="form-control" id="edit-nik" disabled value="${res[i].nik}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-nama">NAMA</label>
                                        <input type="text" class="form-control" id="edit-nama" value="${res[i].nama}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-ttl">TTL</label>
                                        <input type="text" class="form-control" id="edit-ttl" disabled value="${res[i].ttl}" />
                                    </div>

                                    <div class="form-group">
                                        <label for="edit-jenis_kelamin">JENIS KELAMIN</label>
                                        <select class="custom-select" id="edit-jenis_kelamin">
                                            <option value="${res[i].jenis_kelamin}">${res[i].jenis_kelamin}</option>
                                            <option value="LAKI-LAKI">Laki-Laki</option>
                                            <option value="PEREMPUAN">Perempuan</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="edit-kota">KOTA</label>
                                        <input type="text" class="form-control" id="edit-kota" value="${res[i].kota}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-rtrw">RTRW</label>
                                        <input type="text" class="form-control" id="edit-rtrw" value="${res[i].rtrw}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-kelurahan">KELURAHAN</label>
                                        <input type="text" class="form-control" id="edit-kelurahan" value="${res[i].kelurahan}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-kecamatan">KECAMATAN</label>
                                        <input type="text" class="form-control" id="edit-kecamatan" value="${res[i].kecamatan}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="edit-agama">AGAMA</label>
                                        <select class="custom-select" id="edit-agama">
                                            <option value="${res[i].agama}">${res[i].agama}</option>
                                            <option value="ISLAM">ISLAM</option>
                                            <option value="PROTESTAN">PROTESTAN</option>
                                            <option value="KATOLIK">KATOLIK</option>
                                            <option value="HINDU">HINDU</option>
                                            <option value="BUDDHA">BUDDHA</option>
                                            <option value="KHONGHUCU">KHONGHUCU</option>
                                        </select>
                                    </div>
                                    <label for="edit-status_perkawinan">STATUS PERKAWINAN</label>
                                    <div class="form-group">
                                        <select class="custom-select" id="edit-status" >
                                            <option value="${res[i].status}">${res[i].status}</option>
                                            <option value="BELUM MENIKAH">BELUM MENIKAH</option>
                                            <option value="SUDAH MENIKAH">SUDAH MENIKAH</option>
                                            <option value="SUDAH PERNAH MENIKAH">SUDAH PERNAH MENIKAH
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="edit-pekerjaan">PEKERJAAN</label>
                                        <input type="text" class="form-control" id="edit-pekerjaan" value="${res[i].pekerjaan}" />
                                    </div>

                                    <div class="form-group">
                                        <label for="edit-kewarganegaraan">KEWARGANEGARAAN</label>
                                        <select class="custom-select" id="edit-kewarganegaraan">
                                            <option value="${res[i].kewarganegaraan}">${res[i].kewarganegaraan}</option>
                                            <option value="WNI">WNI</option>
                                            <option value="WNA">WNA</option>
                                        </select>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                            
                                        <button type="submit" class="btn btn-primary" onclick="updateData()">Update Data</button>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>


                    <button type="button" class="btn btn-danger" onclick="deleteData(${res[i].nik})">DELETE</button>
</td>
</tr>
                `
                no++;
            }
            document.getElementById("tableData").innerHTML = msgReceipt
        }
    }
    http.send()
}

function updateData() {
    let id = document.getElementById("id").value;
    let nik = document.getElementById("edit-nik").value;
    let nama = document.getElementById("edit-nama").value;
    let ttl = document.getElementById("edit-ttl").value;
    let jenis_kelamin = document.getElementById("edit-jenis_kelamin").value;
    let agama = document.getElementById("edit-agama").value;
    let kota = document.getElementById("edit-kota").value;
    let rtrw = document.getElementById("edit-rtrw").value;
    let kelurahan = document.getElementById("edit-kelurahan").value;
    let kecamatan = document.getElementById("edit-kecamatan").value;
    let status = document.getElementById("edit-status").value;
    let pekerjaan = document.getElementById("edit-pekerjaan").value;
    let kewarganegaraan = document.getElementById("edit-kewarganegaraan").value;

    let jsonStr = JSON.stringify({
        id: id,
        nik: nik,
        nama: nama,
        ttl: ttl,
        jenis_kelamin: jenis_kelamin,
        kota: kota,
        rtrw: rtrw,
        kelurahan: kelurahan,
        kecamatan: kecamatan,
        pekerjaan: pekerjaan,
        agama: agama,
        status: status,
        kewarganegaraan: kewarganegaraan
    });


    fetch('http://localhost:8080/update', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonStr,
    })
        .then(response => response.json())
        .then(() => {
            console.log(jsonStr)
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

getData("http://localhost:8080/Karyawans")