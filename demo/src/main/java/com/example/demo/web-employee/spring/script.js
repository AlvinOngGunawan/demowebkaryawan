const getItems = () => {
    fetch('http://localhost:8080/Karyawans')
        .then(response => response.json())
        .then(data => renderItems(data));
};

const renderItems = (results) => {
    // console.log(results);
    var dataEmployee = document.getElementById("dataEmployee")
    results.forEach((result) => {
        // console.log(result);
        dataEmployee.innerHTML += `<div class="card mb-4 shadow-sm">
                                    <div class="card-header">
                                        <h4 class="my-0 font-weight-normal float-right">
                                            <button class="btn btn-primary btn-sm" data-toggle="modal"
                                                data-target="#${result.nama}Modal">detail</button>
                                        </h4>
                                    </div>
                                    <div class="card-body">
                                        <img src="image/${result.jenis_kelamin}.png" class="rounded-circle" width="150">
                                        <h1 class="card-title pricing-card-title mt-3">${result.nama}</h1>
                                    </div>
                                </div>
                                
                                
                                <div class="modal fade" id="${result.nama}Modal" tabindex="-1" role="dialog" aria-labelledby="${result.nama}ModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="${result.nama}ModalLabel">${result.nama}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="card m-auto text-left" style="width: 25rem;">
                                                    <img src="image/${result.jenis_kelamin}.png" class="card-img-top">
                                                    <div class="card-body">
                                                        <table class="table table-borderless">
                                                            <tr>
                                                                <th>NIK</th>
                                                                <td>${result.nik}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Nama</th>
                                                                <td>${result.nama}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Tempat, tgl lahir</th>
                                                                <td>${result.ttl}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Jenis Kelamin</th>
                                                                <td>${result.jenis_kelamin}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Alamat</th>
                                                                <td>${result.kota}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>RT/RW</th>
                                                                <td>${result.rtrw}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Kelurahan</th>
                                                                <td>${result.kelurahan}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Kecamatan</th>
                                                                <td>${result.kecamatan}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Agama</th>
                                                                <td>${result.agama}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Pekerjaan</th>
                                                                <td>${result.pekerjaan}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>Pekerjaan</th>
                                                                <td>${result.kewarganegaraan}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>`

    });
}

function addData() {
    var nik = document.getElementById("nik").value
    var nama = document.getElementById("nama").value
    var tempat_lahir = document.getElementById("tempat_lahir").value
    var tanggal_lahir = document.getElementById("tanggal_lahir").value
    var jenis_kelamin = document.getElementById("jenis_kelamin").value
    var alamat = document.getElementById("alamat").value
    var rt = document.getElementById("rtrw").value
    var kelurahan = document.getElementById("kelurahan").value
    var kecamatan = document.getElementById("kecamatan").value
    var agama = document.getElementById("agama").value
    var pekerjaan = document.getElementById("pekerjaan").value
    var kewarganegaraan = document.getElementById("kewarganegaraan").value

            var str_json = JSON.stringify({
            nik: nik,
            nama: nama,
            tempat_lahir: tempat_lahir,
            tanggal_lahir: new_tanggalLahir,
            jenis_kelamin: jenis_kelamin,
            alamat: alamat,
            rtrw: rtrw,
            kelurahan: kelurahan,
            kecamatan: kecamatan,
            agama: agama,
            pekerjaan: pekerjaan
        })

        // var jsonParse = JSON.parse(str_json)
        // console.log(jsonParse);

        // fetch post to api
        fetch('http://localhost:8080/addKaryawan', {
                method: 'POST', // or 'PUT'
                headers: {
                    'Content-Type': 'application/json',
                },
                body: str_json,
            })
            .then(response => response.json())
            .then(data => {
                console.log('Success:', data);
            })
            .catch((error) => {
                console.error('Error:', error);
            });

    }

    window.location.href = "/index.html"
getItems();