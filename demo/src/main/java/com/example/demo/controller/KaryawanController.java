package com.example.demo.controller;

import com.example.demo.entity.Karyawan;
import com.example.demo.repository.KaryawanRepository;
import com.example.demo.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class KaryawanController {
    @Autowired
    private KaryawanService Karyawan;
    private KaryawanRepository repository;

    @PostMapping("/addKaryawan")
    public Karyawan addKaryawan(@RequestBody Karyawan karyawan) {
        return Karyawan.saveKaryawan(karyawan);
    }

    @PostMapping("/addKaryawans")
    public List<Karyawan> addKaryawans(@RequestBody List<Karyawan> karyawans) {
        return Karyawan.saveKaryawans(karyawans);
    }

    @GetMapping("/Karyawans")
    public List<Karyawan> findAllKaryawans() {
        return Karyawan.getKaryawans();
    }

    @GetMapping("/Karyawan/{id}")
    public Karyawan findKaryawanById(@PathVariable int id) {
        return Karyawan.getKaryawanById(id);
    }

    @GetMapping("/KaryawanByNama/{nama}")
    public Karyawan findKaryawanByNama(@PathVariable String nama) {
        return Karyawan.getKaryawanByNama(nama);
    }
//    @PutMapping("/update/{nik}")
//    public String updateKaryawan(@PathVariable int nik) {
//        return Karyawan.updateKaryawan(nik);

//    }
    @PutMapping("/update")
    public Karyawan updateKaryawan(@RequestBody Karyawan karyawan) {
        return Karyawan.updateKaryawan(karyawan);
    }


    @DeleteMapping("/delete/{nik}")
    public String deleteKaryawan(@PathVariable int nik) {
        return Karyawan.deleteKaryawan(nik);
    }

    }


